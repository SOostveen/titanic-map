<!-- Navigation -->
<nav class="navbar navbar-success navbar-static-top theme-selected" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="navbar-brand">
            <div>
                <img src="../img/logo.jpg" height="30px"/>
                <span style="vertical-align:middle; color:white">TITANIC</span>
            </div>
        </div>
    </div>
    <!-- /.navbar-header -->

    <div class="navbar-success sidebar" role="navigation" style="background:white">
        <div class="sidebar-nav navbar-collapse">
            <form>
                <select>
                    <option value="a-deck">A-deck</option>
                    <option value="a-deck">B-deck</option>
                    <option value="a-deck">C-deck</option>
                    <option value="a-deck">D-deck</option>
                    <option value="a-deck">E-deck</option>
                    <option value="a-deck">F-deck</option>
                    <option value="a-deck">G-deck</option>
                    <option value="a-deck">Boat-deck</option>
                </select>
            </form>
        </div> 
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>  