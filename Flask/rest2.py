__author__ = "Stephan Oostveen"
from flask import Flask, send_file
from io import BytesIO
from PIL import Image, ImageDraw
from database_connection import *
import random

app = Flask('__name__')

resize_factor = {
    # "deck-name" : (optimized factor, small factor)
    'a': (1, 0.4),
    'b': (0.892, 0.4),
    'c': (1, 0.4),
    'd': (0.904, 0.4),
    'e': (1, 0.4),
    'f': (1, 0.4),
    'g': (1, 0.399),
}


@app.route('/deck/<deckname>/', defaults={'attribute': None, 'value': None})
@app.route('/deck/<deckname>/<attribute>/', defaults={'value': None})
@app.route('/deck/<deckname>/<attribute>/<value>/')
def deckpicture(deckname, attribute, value):
    if deckname not in resize_factor:
        return 'Deck: {0} was not found'.format(deckname)
    img = Image.open('static/images/' + deckname + '-deck-opt.jpg')
    draw = ImageDraw.Draw(img)
    if attribute == None or value == None:
        points = get_default_points(deckname)
    else:
        points = get_points(deckname, attribute, value)
    current_tuple = None
    current_count = 0
    for (x, y, s) in points:
        if current_tuple is None:
            current_tuple = (x, y, s)
            current_count += 1
        elif current_tuple == (x, y, s):
            current_count += 1
            continue
        else:
            (opt_factor, small_factor) = resize_factor[deckname]
            (a, b, c) = current_tuple
            a *= opt_factor
            b *= opt_factor
            for i in range(0, current_count):
                m = random.random()
                m = int(m * 25)
                m = -1 * m if random.random() < 0.5 else m
                draw.ellipse((a + m - 10, b + m - 10, a + m + 10, b + m + 10),
                             fill=(0, 255, 0) if c == 1 else (255, 0, 0))
            current_count = 1
            current_tuple = (x, y, s)
    if current_tuple is not None:
        (opt_factor, small_factor) = resize_factor[deckname]
        (a, b, c) = current_tuple
        a *= opt_factor
        b *= opt_factor
        for i in range(0, current_count):
            m1 = random.random()
            m1 = int(m1 * 25)
            m1 = -1 * m1 if random.random() < 0.5 else m1
            m2 = random.random()
            m2 = int(m2 * 25)
            m2 = -1 * m2 if random.random() < 0.5 else m2
            draw.ellipse((a + m1 - 10, b + m2 - 10, a + m1 + 10, b + m2 + 10),
                         fill=(0, 255, 0) if c == 1 else (255, 0, 0))
    return serve_pil_image(img)


@app.route('/small/deck/<deckname>/', defaults={'attribute': None, 'value': None})
@app.route('/small/deck/<deckname>/<attribute>/', defaults={'value': None})
@app.route('/small/deck/<deckname>/<attribute>/<value>/')
def smalldeckpicture(deckname, attribute, value):
    if deckname not in resize_factor:
        return 'Deck: {0} was not found'.format(deckname)
    img = Image.open('static/images/' + deckname + '-deck-small.jpg')
    draw = ImageDraw.Draw(img)
    if attribute == None or value == None:
        points = get_default_points(deckname)
    else:
        points = get_points(deckname, attribute, value)
    current_tuple = None
    current_count = 0
    for (x, y, s) in points:
        if current_tuple is None:
            current_tuple = (x, y, s)
            current_count += 1
        elif current_tuple == (x, y, s):
            current_count += 1
            continue
        else:
            (opt_factor, small_factor) = resize_factor[deckname]
            (a, b, c) = current_tuple
            a *= small_factor
            b *= small_factor
            for i in range(0, current_count):
                m1 = random.random()
                m1 = int(m1 * 12)
                m1 = -1 * m1 if random.random() < 0.5 else m1
                m2 = random.random()
                m2 = int(m2 * 12)
                m2 = -1 * m2 if random.random() < 0.5 else m2
                draw.ellipse((a + m1 - 4, b + m2 - 4, a + m1 + 4, b + m2 + 4),
                             fill=(0, 255, 0) if c == 1 else (255, 0, 0))
            current_count = 1
            current_tuple = (x, y, s)
    if current_tuple is not None:
        (opt_factor, small_factor) = resize_factor[deckname]
        (a, b, c) = current_tuple
        a *= opt_factor
        b *= opt_factor
        for i in range(0, current_count):
            m1 = random.random()
            m1 = int(m1 * 12)
            m1 = -1 * m1 if random.random() < 0.5 else m1
            m2 = random.random()
            m2 = int(m2 * 12)
            m2 = -1 * m2 if random.random() < 0.5 else m2
            draw.ellipse((a + m1 - 4, b + m2 - 4, a + m1 + 4, b + m2 + 4), fill=(0, 255, 0) if c == 1 else (255, 0, 0))
    return serve_pil_image(img)


def get_default_points(deckname):
    return run_execute_one_query(
        """
            SELECT l.x, l.y, p.Survived
            FROM `passenger-location` as pl, passengers as p, location as l
            WHERE l.id = pl.location_id AND p.PassengerID = pl.passenger_id AND pl.location_id LIKE \'{0}%\'
            ORDER BY l.x ASC
        """.
            format(deckname)
    )


def get_points(deckname, attribute, value):
    return run_execute_one_query(
        """
            SELECT l.x, l.y, p.Survived
            FROM `passenger-location` as pl, passengers as p, location as l
            WHERE l.id = pl.location_id AND p.PassengerID = pl.passenger_id AND pl.location_id LIKE \'{0}%\'
            AND `{1}` = '{2}' ORDER BY l.x ASC
        """.
            format(deckname, attribute, value)
    )


# Creates a byte file that can be returned to the user
def serve_pil_image(pil_img):
    img_io = BytesIO()
    pil_img.save(img_io, 'JPEG', quality=70)
    img_io.seek(0)
    return send_file(img_io, mimetype='image/jpeg')


if __name__ == '__main__':
    app.run(debug=True, port=8080)
