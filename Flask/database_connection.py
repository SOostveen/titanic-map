__author__ = 'Stephan Oostveen'
import MySQLdb
import MySQLdb.cursors


def init_db_conn():
    return MySQLdb.connect(
        host='82.173.112.237',  # your host, usually localhost
        user="honors",  # your username
        passwd="t45504F7EK",  # your password
        db="Kaggle",  # your database
    )


def run_execute_one_query(query, tup=None):
    conn = init_db_conn()
    cur = conn.cursor()

    try:
        if tup:
            cur.execute(query, tup)
        else:
            cur.execute(query)
        conn.commit()
        if not "INSERT" in query:
            return cur.fetchall()
    except Exception as e:
        return handle_error(e)


def handle_error(e):
    print( "Error! " + str(e))
    return "Error! " + str(e)
