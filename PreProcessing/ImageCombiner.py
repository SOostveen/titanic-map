__author__ = "Stephan Oostveen"
from PIL import Image

# Deck names plus the amount of columns the total picture contains
DECK_NAMES = [('a-deck', 23), ('b-deck', 36), ('c-deck', 37), ('d-deck', 36), ('e-deck', 36), ('f-deck', 35),
              ('g-deck', 35), ('boat-deck', 21), ('orlop-deck', 35)]

# Stores temporarily the loaded images in order to combine them later on
image = []

# Loads all the images from the specified deck and stores them in image[] for later use. k is the amount of columns
# the total image consists of.
# Returns the width and height the total image will be.
def load_images(deck, k):
    in_image = 'images/{0}/6-'.format(deck)
    width = 0
    height = 0
    for i in range(0, k):
        image.append([])
        for j in range(0, 5):
            image[i].append(Image.open(in_image + '{0}-{1}.jpg'.format(i, j)))

            (w, h) = image[i][j].size
            if j == 0:
                width += w
            if i == 0:
                height += h
    return width, height


# Constructs the total image out of the small images downloaded from the internet
# Saves the constructed image to disk
def construct_total_image(width, height, deck, k):
    out_image = 'images/{0}.jpg'.format(deck)
    blank_image = Image.new("RGB", (width, height))
    width = 0
    height = 0
    for i in range(0, k):
        for j in range(0, 5):
            (w, h) = image[i][j].size
            blank_image.paste(image[i][j], (width, height))
            height += h

            if j == 4:
                width += w
        height = 0
    blank_image.save(out_image)


# Main function of the ImageCombiner
def main():
    for deck in DECK_NAMES:
        image.clear()
        width, height = load_images(deck[0], deck[1])
        construct_total_image(width, height, deck[0], deck[1])


if __name__ == '__main__':
    main()
