<!DOCTYPE html>
<html lang="en">

<head>
	<script src="../js/jquery-1.8.3.min.js"></script>
	<script src="../js/jquery.elevatezoom.js"></script>
    <?php 
        include '../phpincludes/header.php'; 
    ?>     
</head>

<body>
    <div id="wrapper">
        <?php include '../phpincludes/navigation.php'; ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Deck</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-success">
                        <div class="panel-heading theme-selected">
							Passengers cabin location on board of the Titanic.   
                        </div>
                        <div class="panel-body">
                            <div id="line-chart">
                                <img id="zoom_01" class="img-responsive" src="http://<?php echo $_SERVER['SERVER_NAME'] ?>:8080/small/deck/c/" data-zoom-image="http://<?php echo $_SERVER['SERVER_NAME'] ?>:8080/deck/c/"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>           
        </div>
    </div>
    <script>
    $('#zoom_01').elevateZoom({
        scrollZoom : true,
        zoomWindowPosition: 7
    }); 
</script>
    <?php 
        include '../phpincludes/footer.php';
    ?>  
</body>
</html>
